<?php

namespace LotVantage\ModelViewer;

use Illuminate\Database\Eloquent\Model;

class Finder
{
    /**
     * Find a ModelViewer subclass object to use to view
     * the given Model.
     * @param  Model  $obj 
     * @return ModelViewer
     */
    public function view(Model $obj)
    {
        $class = get_class($obj);
        while ($class !== Model::class) {
            $view_class = "\\App\\ModelViewers\\{$class}Viewer";
            if (class_exists($view_class)) {
                return new $view_class($obj);
            }
            $class = get_parent_class($class);
        }
        return new ModelViewer($obj);
    }
}
