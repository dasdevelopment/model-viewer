<?php

namespace LotVantage\ModelViewer\Facades;

use Illuminate\Support\Facades\Facade;
use LotVantage\ModelViewer\Finder as FinderClass;

/**
 * Using this facade, ModelViewer becomes very simple.
 * 
 * To get all the data you want from a Model object:
 *
 * ModelViewer::view($object)->get(['id', 'name', 'address', 'countInvoices']);
 *
 * Or for just one field:
 *
 * ModelViewer::view($object)->countInvoices;
 *
 * Or to make fine-tuned decisions:
 *
 * $model_view = ModelViewer::view($object);
 *
 * if ($model_view->hasInvoices) {
 *     echo $model_view->countInvoices;
 * }
 *
 * To see what strategies were selected for each field:
 *
 * ModelViewer::view($object)->getHandlerList(['id', 'name', 'address', 'countInvoices']);
 *
 */

class ModelViewer extends Facade
{

    protected static function getFacadeAccessor() 
    { 
        return FinderClass::class; 
    }

}
