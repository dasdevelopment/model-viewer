<?php

namespace LotVantage\ModelViewer;

class Result
{

    private $model_viewer;

    public function __construct(ModelViewer $model_viewer)
    {
        $this->model_viewer = $model_viewer;
    }

    private function keys()
    {
        $keys = array_keys(get_object_vars($this));
        return array_diff($keys, ['model_viewer']);
    }

    public function toJson()
    {
        return json_encode($this);
    }

    public function __toString()
    {
        return $this->toJson();
    }

    public function onto($object)
    {
        foreach ($this->keys() as $field) {
            $object->$field = $this->$field;
        }
        return $object;
    }

    public function toArray()
    {
        $array = get_object_vars($this);
        unset($array['model_viewer']);
        return $array;
    }

    public function get(array $fields, array $vars = [])
    {
        $new = $this->model_viewer->get($fields, $vars);
        $this->onto($new);
        return $new;
    }

    public function getHandlerList(array $fields = null)
    {
        if ($fields === null) {
            $fields = $this->keys();
        }
        return $this->model_viewer->getHandlerList($fields);
    }

    public function getSearchOrderList(array $fields = null)
    {
        if ($fields === null) {
            $fields = $this->keys();
        }
        return $this->model_viewer->getSearchOrderList($fields);
    }
}
