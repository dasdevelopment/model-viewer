<?php

namespace LotVantage\ModelViewer;

use Illuminate\Database\Eloquent\Model;
use View;

class FieldFinder
{
    private $cache;

    public function getCallables($class, array $fields, ModelViewer $viewer)
    {
        $callables = [];
        foreach ($fields as $field) {
            $callables[] = $this->getCallable($class, $field, $viewer);
        }
        return $callables;
    }

    /**
     * Gets information and a Callable to use to produce data for the field.
     * @param  string      $class  
     * @param  string      $field  
     * @param  ModelViewer $viewer 
     * @return array      An array including the name, how we decided to 
     *                    handle the field and a way to call the handler
     *                    code.
     */
    private function getCallable($class, $field, ModelViewer $viewer)
    {
        $forward_slashed_class = str_replace('\\', '/', $class);
        $path = base_path() . '/resources/model_views/' . $forward_slashed_class . '/' . $field . '.blade.php';
        $cache_key = "{$class}::{$field}";
        $search_order = [
            get_class($viewer) . "::{$field}()",
            $path,
            $class . "::{$field}()",
            $class . "->{$field}",
        ];
        if (method_exists($viewer, $field)) {
            // This happens before the cache search because
            // the viewer object cannot be reliably cached. 
            // It may be a different object for each call, 
            // instanciated with different values.
            $call = [$viewer, $field];
            $found = get_class($viewer) . "::{$field}()";
            return [
                'name'  => $field,
                'handler' => $found,
                'call'  => $call,
                'search_order' => $search_order,
            ];
        } elseif (isset($this->cache[$cache_key])) {
            return $this->cache[$cache_key];
        } elseif (file_exists($path)) {
            $call = function ($object, $vars) use ($path) {
                $vars['object'] = $object;
                return View::file($path, $vars)->render();
            };
            $found = $path;
        } elseif (method_exists($class, $field)) {
            $call = function ($object) use ($field) {
                return $object->$field();
            };
            $found = $class . "::{$field}()";
        } else {
            $call = function ($object) use ($field) {
                return $object->$field;
            };
            $found = $class . "->{$field}";
        }
        $callable = [
            'name'  => $field,
            'handler' => $found,
            'call'  => $call,
            'search_order' => $search_order,
        ];
        $this->cache[$cache_key] = $callable;
        return $callable;
    }
}
