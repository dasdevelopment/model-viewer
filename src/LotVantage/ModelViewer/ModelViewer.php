<?php

namespace LotVantage\ModelViewer;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class ModelViewer
{
    protected $object;

    /**
     * Each ModelViewer instance has $this->object it can access
     * @param Model $obj
     */
    public function __construct(Model $obj)
    {
        $this->object = $obj;
    }

    /**
     * Get just one field from get() by accessing $viewer->field
     *
     * @param  string $key the field to access
     *
     * @return mixed      whatever the field, method or blade template
     *                    produced
     * @throws \Exception
     */
    public function __get($key)
    {
        return $this->get([$key])->$key;
    }

    /**
     * Gets the data for the fields given. The fields can be the names of
     * fields, methods, or blade templates
     *
     * @param  array $fields names of fields
     * @param  array $vars   extra data to make available to the handlers
     *
     * @return object          An object with those column names filled in.
     * @throws \Exception
     */
    public function get(array $fields, array $vars = [])
    {
        $this->validate();
        $callables = App::make(FieldFinder::class)->getCallables(get_class($this->object), $fields, $this);
        $data = App::makeWith(Result::class, ['model_viewer' => $this]);
        foreach ($callables as $callable) {
            $name = $callable['name'];
            $data->$name = call_user_func_array($callable['call'], [$this->object, $vars]);
        }
        return $data;
    }

    /**
     * @param array $fields
     *
     * @return array
     * @throws \Exception
     */
    public function getHandlerList(array $fields)
    {
        $this->validate();
        $callables = App::make(FieldFinder::class)->getCallables(get_class($this->object), $fields, $this);
        $handlers = [];
        foreach ($callables as $callable) {
            $handlers[$callable['name']] = $callable['handler'];
        }
        return $handlers;
    }

    /**
     * @param array $fields
     *
     * @return array
     * @throws \Exception
     */
    public function getSearchOrderList(array $fields)
    {
        $this->validate();
        $callables = App::make(FieldFinder::class)->getCallables(get_class($this->object), $fields, $this);
        $search_orders = [];
        foreach ($callables as $callable) {
            $search_orders[$callable['name']] = $callable['search_order'];
        }
        return $search_orders;
    }

    /**
     * @throws \Exception
     */
    private function validate()
    {
        if (!$this->object instanceof Model) {
            throw new \Exception(get_class($this) . ' was not instatiated correctly. Check that the __construct method calls parent::__construct.');
        }
    }
}
