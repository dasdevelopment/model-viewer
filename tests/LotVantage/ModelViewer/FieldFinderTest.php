<?php

namespace LotVantage\ModelViewer;

use TestCase;
use DummyModel;
use App\ModelViewers\DummyModelViewer;

class FieldFinderTest extends TestCase
{

    public function testNew()
    {
        new FieldFinder();
    }

    public function testGetCallables()
    {
        $finder = new FieldFinder();
        $list = $finder->getCallables(DummyModel::class, ['abc', 'methodOne', 'methodABC', 'templateABC'], new DummyModelViewer(new DummyModel()));
        static::assertEquals('abc', $list[0]['name']);
        static::assertEquals('DummyModel->abc', $list[0]['handler']);
        static::assertTrue(is_object($list[0]['call']));
        static::assertEquals('Closure', get_class($list[0]['call']));
        static::assertEquals([
            'App\ModelViewers\DummyModelViewer::abc()',
            base_path() . '/resources/model_views/DummyModel/abc.blade.php',
            'DummyModel::abc()',
            'DummyModel->abc',
        ], $list[0]['search_order']);
        static::assertEquals('methodOne', $list[1]['name']);
        static::assertEquals('App\ModelViewers\DummyModelViewer::methodOne()', $list[1]['handler']);
        static::assertTrue(is_array($list[1]['call']));
        static::assertEquals([
            'App\ModelViewers\DummyModelViewer::methodOne()',
            base_path() . '/resources/model_views/DummyModel/methodOne.blade.php',
            'DummyModel::methodOne()',
            'DummyModel->methodOne',
        ], $list[1]['search_order']);
        static::assertEquals(DummyModelViewer::class, get_class($list[1]['call'][0]));
        static::assertEquals('methodABC', $list[2]['name']);
        static::assertEquals('DummyModel::methodABC()', $list[2]['handler']);
        static::assertTrue(is_object($list[2]['call']));
        static::assertEquals('Closure', get_class($list[2]['call']));
        static::assertEquals([
            'App\ModelViewers\DummyModelViewer::methodABC()',
            base_path() . '/resources/model_views/DummyModel/methodABC.blade.php',
            'DummyModel::methodABC()',
            'DummyModel->methodABC',
        ], $list[2]['search_order']);
        static::assertEquals('templateABC', $list[3]['name']);
        static::assertEquals(base_path() . '/resources/model_views/DummyModel/templateABC.blade.php', $list[3]['handler']);
        static::assertTrue(is_object($list[3]['call']));
        static::assertEquals('Closure', get_class($list[3]['call']));
        static::assertEquals([
            'App\ModelViewers\DummyModelViewer::templateABC()',
            base_path() . '/resources/model_views/DummyModel/templateABC.blade.php',
            'DummyModel::templateABC()',
            'DummyModel->templateABC',
        ], $list[3]['search_order']);
    }

}
