<?php

namespace LotVantage\ModelViewer;

use TestCase;
use DummyModel;
use App\ModelViewers\DummyModelViewer;

class ModelViewerTest extends TestCase
{

    public function testNew()
    {
        new ModelViewer(new DummyModel());
    }

    public function testGetHandlerListDefault()
    {
        $viewer = new ModelViewer(new DummyModel());
        $list = $viewer->getHandlerList(['abc', 'def']);
        static::assertEquals(
            [
                'abc' => 'DummyModel->abc',
                'def' => 'DummyModel->def',
            ],
            $list
        );
    }

    public function testGetSearchOrderListDefault()
    {
        $viewer = new ModelViewer(new DummyModel());
        $list = $viewer->getSearchOrderList(['abc', 'def']);
        static::assertEquals(
            [
                'abc' => [
                    0 => 'LotVantage\\ModelViewer\\ModelViewer::abc()',
                    1 => base_path() . '/resources/model_views/DummyModel/abc.blade.php',
                    2 => 'DummyModel::abc()',
                    3 => 'DummyModel->abc',
                ],
                'def' => [
                    0 => 'LotVantage\\ModelViewer\\ModelViewer::def()',
                    1 => base_path() . '/resources/model_views/DummyModel/def.blade.php',
                    2 => 'DummyModel::def()',
                    3 => 'DummyModel->def',
                ],
            ],
            $list
        );
    }

    public function testGetDefault()
    {
        $viewer = new ModelViewer(new DummyModel([
            'abc' => 123,
            'def' => 'ghi',
        ]));
        $result = $viewer->get(['abc', 'def']);
        static::assertEquals(
            [
                'abc' => 123,
                'def' => 'ghi',
            ],
            $result->toArray()
        );
    }

    public function testGetKeyDefault()
    {
        $viewer = new ModelViewer(new DummyModel([
            'abc' => 123,
            'def' => 'ghi',
        ]));
        $result = $viewer->get(['abc', 'def']);
        static::assertEquals(123, $viewer->abc);
        static::assertEquals('ghi', $viewer->def);
    }

    /**
     * Adding data to the call for a model key shouldn't have any effect.
     */
    public function testGetKeyDefaultWithData()
    {
        $viewer = new ModelViewer(new DummyModel([
            'abc' => 123,
            'def' => 'ghi',
        ]));
        $result = $viewer->get(['abc', 'def'], ['should' => 'not have an affect']);
        static::assertEquals(123, $viewer->abc);
        static::assertEquals('ghi', $viewer->def);
    }

    public function testGetHandlerListModelViewerMethods()
    {
        $viewer = new DummyModelViewer(new DummyModel());
        $list = $viewer->getHandlerList(['methodOne', 'methodTwo']);
        static::assertEquals(
            [
                'methodOne' => 'App\ModelViewers\DummyModelViewer::methodOne()',
                'methodTwo' => 'App\ModelViewers\DummyModelViewer::methodTwo()',
            ],
            $list
        );
    }

    public function testGetSearchOrderListModelViewerMethods()
    {
        $viewer = new DummyModelViewer(new DummyModel());
        $list = $viewer->getSearchOrderList(['methodOne', 'methodTwo']);
        static::assertEquals(
            [
                'methodOne' => [
                    0 => 'App\\ModelViewers\\DummyModelViewer::methodOne()',
                    1 => base_path() . '/resources/model_views/DummyModel/methodOne.blade.php',
                    2 => 'DummyModel::methodOne()',
                    3 => 'DummyModel->methodOne',
                ],
                'methodTwo' => [
                    0 => 'App\\ModelViewers\\DummyModelViewer::methodTwo()',
                    1 => base_path() . '/resources/model_views/DummyModel/methodTwo.blade.php',
                    2 => 'DummyModel::methodTwo()',
                    3 => 'DummyModel->methodTwo',
                ],
            ],
            $list
        );
    }

    public function testGetModelViewerMethods()
    {
        $viewer = new DummyModelViewer(new DummyModel());
        $result = $viewer->get(['methodOne', 'methodTwo']);
        static::assertEquals(
            [
                'methodOne' => 'MethodOned',
                'methodTwo' => 'MethodTwod',
            ],
            $result->toArray()
        );
    }

    public function testGetModelViewerMethodWithData()
    {
        $viewer = new DummyModelViewer(new DummyModel());
        $result = $viewer->get(['methodThree'], ['a' => 20, 'b' => 21]);
        static::assertEquals(
            [
                'methodThree' => '[[],{"a":20,"b":21}]',
            ],
            $result->toArray()
        );
    }

    public function testGetHandlerListModelMethods()
    {
        $viewer = new DummyModelViewer(new DummyModel());
        $list = $viewer->getHandlerList(['methodABC', 'methodDEF']);
        static::assertEquals(
            [
                'methodABC' => 'DummyModel::methodABC()',
                'methodDEF' => 'DummyModel::methodDEF()',
            ],
            $list
        );
    }

    public function testGetSearchOrderListModelMethods()
    {
        $viewer = new DummyModelViewer(new DummyModel());
        $list = $viewer->getSearchOrderList(['methodABC', 'methodDEF']);
        static::assertEquals(
            [
                'methodABC' => [
                    0 => 'App\\ModelViewers\\DummyModelViewer::methodABC()',
                    1 => base_path() . '/resources/model_views/DummyModel/methodABC.blade.php',
                    2 => 'DummyModel::methodABC()',
                    3 => 'DummyModel->methodABC',
                ],
                'methodDEF' => [
                    0 => 'App\\ModelViewers\\DummyModelViewer::methodDEF()',
                    1 => base_path() . '/resources/model_views/DummyModel/methodDEF.blade.php',
                    2 => 'DummyModel::methodDEF()',
                    3 => 'DummyModel->methodDEF',
                ],
            ],
            $list
        );
    }

    public function testGetModelMethods()
    {
        $viewer = new DummyModelViewer(new DummyModel());
        $result = $viewer->get(['methodABC', 'methodDEF']);
        static::assertEquals(
            [
                'methodABC' => 'Method ABC',
                'methodDEF' => 'Method DEF',
            ],
            $result->toArray()
        );
    }

    /**
     * Adding data to the call for a model method shouldn't have any effect.
     */
    public function testGetModelMethodWithData()
    {
        $viewer = new DummyModelViewer(new DummyModel());
        $result = $viewer->get(['methodABC'], ['should' => 'not have an affect']);
        static::assertEquals(
            [
                'methodABC' => 'Method ABC',
            ],
            $result->toArray()
        );
    }

    public function testGetHandlerListTemplates()
    {
        $viewer = new DummyModelViewer(new DummyModel());
        $list = $viewer->getHandlerList(['templateABC', 'templateXYZ']);
        static::assertEquals(
            [
                'templateABC' => base_path() . '/resources/model_views/DummyModel/templateABC.blade.php',
                'templateXYZ' => base_path() . '/resources/model_views/DummyModel/templateXYZ.blade.php',
            ],
            $list
        );
    }

    public function testGetSearchOrderListTemplates()
    {
        $viewer = new DummyModelViewer(new DummyModel());
        $list = $viewer->getSearchOrderList(['templateABC', 'templateXYZ']);
        static::assertEquals(
            [
                'templateABC' => [
                    0 => 'App\\ModelViewers\\DummyModelViewer::templateABC()',
                    1 => base_path() . '/resources/model_views/DummyModel/templateABC.blade.php',
                    2 => 'DummyModel::templateABC()',
                    3 => 'DummyModel->templateABC',
                ],
                'templateXYZ' => [
                    0 => 'App\\ModelViewers\\DummyModelViewer::templateXYZ()',
                    1 => base_path() . '/resources/model_views/DummyModel/templateXYZ.blade.php',
                    2 => 'DummyModel::templateXYZ()',
                    3 => 'DummyModel->templateXYZ',
                ],
            ],
            $list
        );
    }

    public function testGetListTemplates()
    {
        $viewer = new DummyModelViewer(new DummyModel());
        $result = $viewer->get(['templateABC', 'templateXYZ']);
        static::assertEquals(
            [
                'templateABC' => '<div>Template ABC</div>',
                'templateXYZ' => '<div>Template XYZ</div>',
            ],
            $result->toArray()
        );
    }

    public function testGetListTemplateWithData()
    {
        $viewer = new DummyModelViewer(new DummyModel());
        $result = $viewer->get(['templateDEF'], ['a' => 20, 'b' => 21, 'c' => null]);
        static::assertEquals(
            [
                'templateDEF' => '<div>[[],20,21,null]</div>',
            ],
            $result->toArray()
        );
    }
}
