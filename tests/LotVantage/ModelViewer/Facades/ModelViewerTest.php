<?php

namespace LotVantage\ModelViewer\Facades;

use TestCase;
use DummyModel;
use DummyModelSub;
use DummyModel2;
use App\ModelViewers\DummyModelViewer;
use LotVantage\ModelViewer\ModelViewer as TrueViewer;

class ModelViewerTest extends TestCase
{


    public function testViewDummyModel()
    {
        $viewer = ModelViewer::view(new DummyModel());
        static::assertEquals(DummyModelViewer::class, get_class($viewer));
    }

    public function testViewDummyModelSubclass()
    {
        $viewer = ModelViewer::view(new DummyModelSub());
        static::assertEquals(DummyModelViewer::class, get_class($viewer));
    }

    public function testViewDummyModel2()
    {
        $viewer = ModelViewer::view(new DummyModel2());
        static::assertEquals(TrueViewer::class, get_class($viewer));
    }
}
