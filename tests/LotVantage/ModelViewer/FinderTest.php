<?php

namespace LotVantage\ModelViewer;

use TestCase;
use DummyModel;
use DummyModelSub;
use DummyModel2;
use App\ModelViewers\DummyModelViewer;

class FinderTest extends TestCase
{

    public function testNew()
    {
        new Finder();
    }

    public function testViewDummyModel()
    {
        $viewer = (new Finder())->view(new DummyModel());
        static::assertEquals(DummyModelViewer::class, get_class($viewer));
    }

    public function testViewDummyModelSubclass()
    {
        $viewer = (new Finder())->view(new DummyModelSub());
        static::assertEquals(DummyModelViewer::class, get_class($viewer));
    }

    public function testViewDummyModel2()
    {
        $viewer = (new Finder())->view(new DummyModel2());
        static::assertEquals(ModelViewer::class, get_class($viewer));
    }
}
