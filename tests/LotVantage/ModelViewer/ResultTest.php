<?php

namespace LotVantage\ModelViewer;

use TestCase;
use DummyModel;
use App\ModelViewers\DummyModelViewer;

class ResultTest extends TestCase
{

    public function testNew()
    {
        new Result(new DummyModelViewer(new DummyModel()));
    }

    public function testToJson()
    {
        $result = new Result(new DummyModelViewer(new DummyModel()));
        $result->abc = 123;
        static::assertEquals('{"abc":123}', $result->toJson());
    }

    public function testToString()
    {
        $result = new Result(new DummyModelViewer(new DummyModel()));
        $result->abc = 123;
        static::assertEquals('{"abc":123}', "$result");
    }

    public function testOnto()
    {
        $result1 = new Result(new DummyModelViewer(new DummyModel()));
        $result1->abc = 123;
        $result2 = new Result(new DummyModelViewer(new DummyModel()));
        $result2->def = 456;
        $result1->onto($result2);
        static::assertEquals(123, $result2->abc);
    }

    public function testToArray()
    {
        $result = new Result(new DummyModelViewer(new DummyModel()));
        $result->abc = 123;
        static::assertEquals(['abc' => 123], $result->toArray());
    }

    public function testGet()
    {
        $result = new Result(new DummyModelViewer(new DummyModel()));
        $result->abc = 123;
        $result = $result->get(['templateABC']);
        static::assertEquals(
            [
                'abc' => 123,
                'templateABC' => '<div>Template ABC</div>',
            ], 
            $result->toArray()
        );
    }

    public function testGetHandlerList()
    {
        $result = new Result(new DummyModelViewer(new DummyModel()));
        $result->abc = 123;
        static::assertEquals(['abc' => 'DummyModel->abc'], $result->getHandlerList());
    }

    public function testGetHandlerListWithFields()
    {
        $result = new Result(new DummyModelViewer(new DummyModel()));
        $result->abc = 123;
        static::assertEquals(['def' => 'DummyModel->def'], $result->getHandlerList(['def']));
    }

    public function testGetSearchOrderList()
    {
        $result = new Result(new DummyModelViewer(new DummyModel()));
        $result->abc = 123;
        $list = $result->getSearchOrderList();
        static::assertEquals(
            [
                'abc' => [
                    0 => 'App\\ModelViewers\\DummyModelViewer::abc()',
                    1 => base_path() . '/resources/model_views/DummyModel/abc.blade.php',
                    2 => 'DummyModel::abc()',
                    3 => 'DummyModel->abc',
                ],
            ], 
            $list
        );
    }

    public function testGetSearchOrderListWithFields()
    {
        $result = new Result(new DummyModelViewer(new DummyModel()));
        $result->abc = 123;
        $list = $result->getSearchOrderList(['def']);
        $list = $result->getSearchOrderList();
        static::assertEquals(
            [
                'abc' => [
                    0 => 'App\\ModelViewers\\DummyModelViewer::abc()',
                    1 => base_path() . '/resources/model_views/DummyModel/abc.blade.php',
                    2 => 'DummyModel::abc()',
                    3 => 'DummyModel->abc',
                ],
            ], 
            $list
        );
    }
}
