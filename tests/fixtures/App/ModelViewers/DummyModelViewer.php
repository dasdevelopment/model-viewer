<?php

namespace App\ModelViewers;

use LotVantage\ModelViewer\ModelViewer;
use DummyModel;

class DummyModelViewer extends ModelViewer
{

    public function methodOne()
    {
        return "MethodOned";
    }

    public function methodTwo()
    {
        return "MethodTwod";
    }

    public function methodThree($object, array $vars = [])
    {
        return json_encode([$object, $vars]);
    }
}
