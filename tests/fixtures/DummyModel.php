<?php

use Illuminate\Database\Eloquent\Model;

class DummyModel extends Model
{
    protected $guarded = [];

    public function methodABC()
    {
        return 'Method ABC';
    }

    public function methodDEF()
    {
        return 'Method DEF';
    }
}
