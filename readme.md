Model Viewer
===

Build an object of data about a `Model` magically using Blade templates and class methods.

## Requirement

* Laravel or Lumen 5.1+
* Blade templating engine

## Installation

The package can be installed via Composer by requiring the "lotvantage/model-viewer": "dev-master" package in your project's composer.json.

```json
{
    "require": {
        "lotvantage/model-viewer": "1.0.*"
    },
    "repositories": [
        {
            "type": "vcs",
            "url":  "git@gitlab.thatsus.com:dkuck/model-viewer.git"
        }
    ]
}
```

### Usage

```php
use LotVantage\ModelViewer\Facades\ModelViewer;
use App\Models\SomeModel;

$model = SomeModel::first();
$result = ModelViewer::view($model)->get(['id', 'name', 'deleteButton', 'smallDiv']);
```
This code returns an object with fields `id`, `name`, `deleteButton`, and `smallDiv`.

The `ModelViewer` will search for handlers for each field. Example: `id`

1. \App\ModelViewers\App\Models\SomeModelViewer::id()
2. \App\Models\SomeModel::id()
3. resources/model_views/App/Models/SomeModel/id.blade.php
4. \App\Models\SomeModel->id

The search is conducted in order and stops as soon as a handler is found.

To test what handlers are found for each field, you may call `getHandlerList`.

```php
$list = ModelViewer::view($model)->getHandlerList(['id', 'name', 'deleteButton', 'smallDiv']);
print_r($list);
```

To see all the handlers that were searched for, you may call `getSearchOrderList`.

```php
$list = ModelViewer::view($model)->getSearchOrderList(['id', 'name', 'deleteButton', 'smallDiv']);
print_r($list);
```

To attach the results to some other object, use the `onto` method of the result object.

```php
$some_object = someProcess();
$result = ModelViewer::view($model)->get(['id', 'name', 'deleteButton', 'smallDiv']);
$result->onto($some_object);
```

In Blade templates, the Model object is exposed as the `$object` variable.

```php
<div>{{ $object->title }}</div>
```

The `get` method accepts an optional second parameter, an array of variables to inject 
into the handlers. This is used for every field requested. Only some handlers
use it.

1. \App\ModelViewers\App\Models\SomeModelViewer::id($object, $vars)
2. \App\Models\SomeModel::id() // not used
3. resources/model_views/App/Models/SomeModel/id.blade.php // each key becomes a variable
4. \App\Models\SomeModel->id // not used
